+++
title = "Disclaimer"
date = "2015-10-01"
+++
# Disclaimer

## Any resemblance to actual breweries, living or dead, is purely coincidental.

Smith Basement Brewing Company is not a real brewery. It isn't even a company. 

I created this website to share recipes and some of the things I've learned in my hobby of home brewing.

If you have any questions, comments or critiques about any of the information presented here, I intend to add a comment form, but for now it is not available.

