---
type: "post"
draft: 
author: "Jim Smith"
description: "The idea to brew this beer came to me during my thru-hike of the Appalachian Trail. The beer is supposed to be well-attenuated, bitter yet malty, clean, and smooth, amber-to copper-colored."
keywords: [
    "Altbier",
    "German",
]
categories: [
    "Recipes",
]
tags: [
    "Appalachian Trail Series",
    "Brew it Again",
    "Altbier",
]
title: "Killington Peak Düsseldorf Altbier"
date: 2018-02-25T14:58:31-05:00
banner: "/images/killington-peak.jpg"
thumbnail: "/images/killington-thumb.jpg"

---


An altbier is sometimes considered to be a hybrid beer. That's because it uses an ale yeast, but has many characteristics of a lager.

This style is said to have originated in Düsseldorf before bottom fermenting yeasts were available. 

Though top fermenting, the fermentation temperature is usually kept on the cooler than most ales, thus lending many characteristics found in lagers.

This style is described in the [Beer Certified Judges Program guidelines](https://www.bjcp.org/docs/2015_Guidelines_Beer.pdf) as being "well-attenuated, bitter yet malty, clean, and smooth, amber-to copper-colored".

### Inspiration

The idea to brew this beer came to me during my thru-hike of the Appalachian Trail. On a couple occassionals while going into a town to resupply, I had the opportunity to purchase [Long Trail Ale](http://longtrail.com/beers/long-trail-ale%C2%AE), which is brewed near the trail in Bridgewater Corners, Vt. by Long Trail Brewing Company.

Long Trail's version is hoppy, but doesn't the bright and malty character expected of this style. 

The beer I brewed is named for Killington Peak, a mountain that is just a few miles down the road from Long Trail's brewery. The Appalachian Trail does not summit this mountain, but there is a short side trail to the top. The photo above on the right was taken on the day I climbed to the summit with two of my best hiking friends, Mechanic and Pippi.



### Ingredients

```
  Total Grain Weight: 13 lbs 4.0 oz	Total Hops: 2.00 oz oz.

  Amt                   Name                                     Type          #        %/IBU         
  10 lbs 8.0 oz         Pale Malt, Maris Otter (3.0 SRM)         Grain         1        79.2 %        
  1 lbs                 Aromatic Malt (26.0 SRM)                 Grain         2        7.5 %         
  1 lbs                 Wheat - Red Malt (Briess) (2.3 SRM)      Grain         3        7.5 %         
  8.0 oz                Caramel/Crystal Malt - 60L (60.0 SRM)    Grain         4        3.8 %         
  4.0 oz                Chocolate Malt (350.0 SRM)               Grain         5        1.9 %         
```

### Mash
```  
  Name              Description                             Step Temperat Step Time     
  Saccharification  Add 41.94 qt of water at 158.3 F        152.1 F       75 min        
  Mash Out          Heat to 168.0 F over 7 min              168.0 F       10 min        
```
  
### Boil
```  
  Est Pre_Boil Gravity: 1.041 SG	Est OG: 1.055 SG
  
  Amt                   Name                                     Type          #        %/IBU         
  0.50 oz               Cascade [5.50 %] - Boil 90.0 min         Hop           6        8.5 IBUs      
  0.50 oz               Centennial [10.00 %] - Boil 90.0 min     Hop           7        15.4 IBUs     
  0.50 oz               Cascade [5.50 %] - Boil 5.0 min          Hop           8        1.6 IBUs      
  0.50 oz               Centennial [10.00 %] - Boil 5.0 min      Hop           9        2.9 IBUs      
```
### Fermentation
```
  Dusseldorf Alt Yeast  White Labs WLP036 (with 1 liter starter)
  
  Ferment at 65°
  
  Final gravity of 1.16 reached in 6 days.
```

Brewed on January 13, 2018

Packaged on January 16, 2018


### Tasting Notes

I got impatient and rushed this beer. As soon as it reached final gravity I kegged it. Though it ended up tasting very close to what I wan't, I know that a little more patience would have made it better. 

The next time I brew this beer, and I definitely will, I'll give it more time to cold condition it.

I also want to work on the water chemistry. This batch was made using city water with no treatmetn, other than running it through a charcoal filter and letting sit overnight in the kettle to minimize the chloramines. 