---
type: "page"
draft: true
author: "Jim Smith"
description: "description"
keywords: [
    "key", 
    "words",
]
categories: [
    "topic 1",
]
tags: [
    "one", 
    "two",
]
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
banner: "http://placehold.it/1024x300"
thumbnail: "http://placehold.it/80x80"
---

